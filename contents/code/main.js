var setActivity = function(activity) {
    callDBus("org.kde.ActivityManager", "/ActivityManager/Activities", "org.kde.ActivityManager.Activities", "SetCurrentActivity", activity, function() {});
};

var changeActivity = function() {
    if (workspace.currentDesktop == 1)
        return setActivity("83df0a75-386c-457b-9b3c-748599dd8a27");
    if (workspace.currentDesktop == 2)
        return setActivity("897ddc09-b944-4f1c-bfb6-3223564f86b9");
    if (workspace.currentDesktop == 3)
        return setActivity("cdaa8bf7-56b7-4062-ba2d-86ce9d2337de");
    if (workspace.currentDesktop == 4)
        return setActivity("7e98ea7e-373b-4db1-977b-2db449a067d0");
    if (workspace.currentDesktop == 5)
        return setActivity("38479e04-35ff-40ea-92da-98bc2c29a8a6");
    if (workspace.currentDesktop == 6)
        return setActivity("ad8c5104-be11-4750-b15e-75f68d06c7b8");
    if (workspace.currentDesktop == 7)
        return setActivity("0ff1e000-3c4a-49f2-bcc5-ab3d6e39d310");
    if (workspace.currentDesktop == 8)
        return setActivity("0ff1e000-3c4a-49f2-bcc5-ab3d6e39d310");
    if (workspace.currentDesktop == 9)
        return setActivity("1265da5a-f803-49c1-8ab6-ae8ca64ec83f");
    if (workspace.currentDesktop == 9)
        return setActivity("83df0a75-386c-457b-9b3c-748599dd8a27");

};

workspace.currentDesktopChanged.connect(changeActivity);
